// Homework_17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>

class Vector
{
private:
    float x;
    float y;
    float z;

    float length;
public:
    Vector() : x(1), y(1), z(0), length(5.5)
    {};
    
    void ToConsole()
    {
        std::cout << x << " :" << y << " :" << z;
    }
    
    float GetLength()
    {
        length = std::sqrt(x * x + y * y + z * z);
    
        return length;
    };

    void SetCoordinates(float _x, float _y, float _z )
    {
        x = _x;
        y = _y;
        z = _z;
    };

    void SetLength(float _length)
    {
        length = _length;
    };
};


class MaStuff
{
private:
    int a;
    std::string name;
public:
    Vector Pos;
    MaStuff() : a(1), name("Default")
    {}
    void ToConsole()
    {
        std::cout << a << " : " << name << " : coordinates (";
        Pos.ToConsole();
        std::cout << ")  length : " << Pos.GetLength();
 
    }

    void SetName(std::string _name)
    {
        name = _name;
    }


};


int main()
{
    MaStuff stuff01;

    stuff01.ToConsole();

    std::cout << "\n" << "\n" << "\n";
    
    std::string str = "Default";
    std::cout << "Enter new name : ";
    std::cin >> str;
    

    stuff01.SetName(str);
    stuff01.Pos.SetLength(17.75);
    stuff01.Pos.SetCoordinates(3.5, 2.2, 10);

    stuff01.ToConsole();

    std::cout << "\n" << "\n" << "\n";
   

   


      
}

